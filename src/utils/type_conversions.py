import math
import numpy as np
import torch
from PIL import Image, ImageOps

def to_numpy(array):
    if isinstance(array, np.ndarray):
        return array 
    elif isinstance(array, torch.Tensor):
        np_array = array.cpu().detach().numpy()
        return np_array
    elif isinstance(array, Image.Image):
        np_array = np.asarray(array)
        return np_array
    else:
        raise ValueError("Invalid data provided")

def to_torch(array):
    if isinstance(array, np.ndarray):
        tensor = torch.Tensor(array)
        return tensor
    elif isinstance(array, torch.Tensor):
        return array
    elif isinstance(array, Image.Image):
        # torchvision transforms failed for some reason. This is a workaround
        tensor = np.asarray(array)
        tensor = torch.Tensor(tensor)
        return tensor
    else:
        raise ValueError("Invalid data provided")

def to_PIL(array):
    if isinstance(array, np.ndarray):
        im = Image.fromarray(array)
        return im
    elif isinstance(array, torch.Tensor):
        nparr = to_numpy(array)
        return to_PIL(nparr)
    elif isinstance(array, Image.Image):
        return array
    else:
        raise ValueError("Invalid data provided")

def to_grayscale(array):
    if isinstance(array, np.ndarray):
        assert len(array.shape) == 3 # HWC
        if array.shape[2] == 1:
            return array
        elif array.shape[2] == 3:
            # Convert to grayscale
            im = to_PIL(array)
            gray = to_grayscale(im)
            return to_numpy(gray)
        else:
            raise ValueError("ERROR: Non standard image format provided: {}".format(array.shape))
    elif isinstance(array, torch.Tensor):
        nparr = to_numpy(array)
        npgray = to_graycale(nparr)
        return to_torch(npgray)
    elif isinstance(array, Image.Image):
        gray = ImageOps.grayscale(array) 
        return gray

def chw_to_hwc(tensor):
    permuted_tensor = None
    if len(tensor.shape) == 4:
        print("4")
        permuted_tensor = tensor.permute((0, 2, 3, 1))
    elif len(tensor.shape) == 3:
        permuted_tensor = tensor.permute((1, 2, 0))
    else:
        raise ValueError("ERROR: Invalid shape: {} - Only [N,C,W,H] or [C,W,H] tensor image representations are valid".format(tensor.shape))

    return permuted_tensor
